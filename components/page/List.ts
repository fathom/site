import {SFC, Fragment} from 'react'
import h from 'react-hyperscript'
import styled from 'styled-components'

import {Desktop, Mobile} from '../media'

export const List = styled('div')`
display: grid;
grid-gap: 50px;

${Desktop} {
  grid-template-columns: auto auto auto auto;
}

${Mobile} {
  grid-template-rows: auto auto auto auto;
}
`

export const ListItem:SFC<{img: string, href?:string}> = (props) =>{
  return h(Link, {href: props.href}, [
    h(ListItemTitleContainer, [
      h(ListImg, {src: props.img}),
    ]),
    h(Fragment, [props.children])
  ])
}

const Link = styled('a')`
text-decoration: none;
color: black;
`


const ListItemTitleContainer = styled('div')`
display: grid;

${Desktop} {
 grid-template-rows: auto auto;
}

${Mobile} {
  grid-template-columns: 2em auto;
}
`

const ListImg = styled('img')`
height: 100px;
width: auto;

${Mobile} {
order: 2;
}
`
