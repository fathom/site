import styled from 'styled-components'

import {Mobile, Desktop} from '../media'

// Main should contain one Link and one Block component

export const Main = styled('div')`
display: grid;
height: 80vh;
padding-bottom: 10vh;
font-size: 1.2em;

${Desktop} {
grid-template-columns: auto auto;
align-items: end;
}

${Mobile} {
grid-template-rows: auto auto;
align-items: center;
}
`

export const Links = styled('div')`
display:grid;
${Desktop} { text-align: right; }
align-content: space-between;
`

export const Block = styled('div')<{noBorder?:boolean}>`
margin: 4em 0px;
padding-top: 1em;


border-top: ${props => {
  if(props.noBorder) return 'none;'
  return 'solid;'
}};

border-color: #E8E8E8;

${Desktop} {
  max-width: 700px;
}
`

export const Intro = styled('div')`
${Desktop} {
 max-width: 500px;
}
`
