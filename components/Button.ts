import styled from 'styled-components'

export const Button = styled('a')`
display: inline-block;
text-transform: uppercase;
text-decoration: none;
font-weight: bold;
font-size: 1.2em;

color: white;
word-spacing: 4px;
background-color: black;
border-radius: 8px;
padding: 10px 30px;

margin: 10px 0px;
`
