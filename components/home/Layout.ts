import {SFC} from 'react'
import h from 'react-hyperscript'
import styled from 'styled-components'

import {Mobile, Desktop} from '../media'

import {Layout as MainLayout} from '../Layout'

// Layout
export const Layout:SFC = (props) => {
  return h(MainLayout, {title: 'home'}, [
    h(Main, [props.children])
  ])
}

const Main = styled('div')`
display:grid;
grid-gap: 40px;

${Desktop} {
grid-template-columns: repeat(3, 1fr);
}
${Mobile} {
grid-template-rows: auto auto auto;
padding-right: 100px;
}
`
