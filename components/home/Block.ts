import {SFC} from 'react'
import h from 'react-hyperscript'
import styled from 'styled-components'

import {Mobile, Desktop} from '../media'

export const Block:SFC<{pos: 1 | 2 | 3}> = (props) => {
  return h(Column, props, [
    h(ContentBlock, props, [
      h(Content, [
        props.children
      ]),
      props.pos === 3 ? null : h(Divider)
    ])
  ])
}

const Column= styled('div')<{pos: 1|2|3}>`
${Desktop} {
display:grid;
margin-top: ${props => {
  switch(props.pos) {
    case 1: return '0vh;'
    case 2: return '30vh;'
    case 3: return '60vh;'
  }
}}
}
`
const ContentBlock = styled('div')<{pos: 1|2|3}>`
display:grid;

${props => {
if(props.pos !== 3) {
  return `
    ${Desktop} {
      grid-template-columns: auto 90px;
    }

    ${Mobile} {
      grid-template-rows: auto 10px;
    }
  `
  }
}}
`

const Content = styled('div')`
${Mobile} {
padding-right: 2.5em;
}
`

const Divider = styled('div')`
${Desktop} {
border-right: 2px solid;
border-top: 2px solid;
height: calc(30vh + 1em);
margin-top: 2.5em;
}

${Mobile} {
border-bottom: 2px solid;
border-right: 2px solid;
padding-top: 25px;
}
`
