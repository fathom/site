import {SFC} from 'react'
import h from 'react-hyperscript'
import styled from 'styled-components'
import Link from 'next/link'

export const Title:SFC<{href: string}> = (props) => {
  return h(Link, {
    ...props,
    prefetch: true,
    passHref: true,
  }, h(TitleContainer, [
    h(TitleH1, [props.children]),
    h(Image, {
      src:'/static/arrow icon.svg',
    })
  ]))
}

const TitleContainer = styled('a')`
cursor: pointer;
color: black;
text-decoration: none;
display: grid;
grid-template-columns: fit-content(50%) auto;
align-items: center

&:hover {
text-decoration: underline;
cursor: pointer;
}

`

const TitleH1 = styled('h1')`
text-transform: uppercase;
margin: 0.25em 0px;
`

const Image = styled('img')`
height: 2em;
width: auto;
padding-left: 30px;
`
