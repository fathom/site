import styled from 'styled-components'

export const Highlight = styled('span')`
padding: 2px 2px;

background-color: #d7f4f4;

border-radius: 2px;
text-decoration-line: overline underline;
text-decoration: wavy;
`

// const colors = [
//   "#ffd299;",
//   "#84d8bf;",
//   "#d88b94;",
//   "#aadbf7;",
//   "#bbf7c3;"
// ]
// ${() => {
//   let index = Math.floor(Math.random() * colors.length)
//   return colors[index]
// }}
