import {SFC} from 'react'
import h from 'react-hyperscript'
import styled from 'styled-components'

import Head from 'next/head'
import Link from 'next/link'

import {Mobile, Desktop} from './media'

export const Layout:SFC<{title: string}> = (props) => {
  return h(App,[
    //@ts-ignore
    h(Head, {},
      h('title', 'fathom: ' + props.title)
    ),
    h(Links, [
      h(Link, {href:'/'}, h('a', 'home')),
      h(Link, {href:'/blog'}, h('a', 'blog')),
      h(Link, {href:'https://fns.fathom.network'}, h('a', 'fns')),
      h(Link, {href:'https://gitlab.com/fathom'}, h('a', 'gitlab')),
      h(Link, {href:'https://tinyletter.com/FathomNetwork'}, h('a', 'newsletter')),
    ]),
    h('div', [
      props.children,
      h(Footer, '© Learning Futures Inc 2020')
    ])
  ])
}

let Footer = styled('div')`
text-align: right;
color: grey;
`


let App = styled('div')`
display: grid;
grid-template-rows: fit-content(50%) auto;
grid-gap: 10px;
font-family: 'Nunito', sans-serif;
font-size: 18px;
padding: 10px 20px;
margin:auto;
max-width: 1400px;
height: 100vh;
box-sizing: border-box;

p {
font-size 1.05em;
}

h1 {
  font-size: 3em;
  font-family: 'Roboto', sans-serif;
}

h2, h3, h4, h5 {
font-family: 'Roboto', sans-serif;
}

${Mobile} {
font-size: 2em;
padding: 50px;
}
`

let Links = styled('div')`
font-family: 'Roboto', sans-serif;
display:grid;
text-transform: uppercase;
grid-template-columns: repeat(5, auto);
padding-top: 10px;
padding-bottom: 30px;

a {
  font-size: 20px;
  text-decoration: none;
  color: black;
  font-weight: bold;

  &:hover {
    text-decoration: underline;
  }

  ${Mobile} {
    font-size: 1em;
  }
}

${Desktop} {
  justify-self: end;
  justify-content: right;
  grid-gap: 50px;
}

${Mobile} {
  grid-gap: 10px;
  justify-content: space-between;
}
`
