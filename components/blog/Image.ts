import styled from 'styled-components'
import h from 'react-hyperscript'


export const Image:React.SFC<{caption:string, src: string, width?: string}> = (props) => {
  return h(Figure, [
    h('img', {...props}),
    h(Caption, props.caption)
  ])
}

const Caption = styled('figcaption')`
text-align: center;
font-style: italic;
font-size: 0.9em;
`

const Figure = styled('figure')`
background-color: palevioletred;
border-radius: 5px;
display: inline-block;
padding: 10px;
margin: 0px;
}
`
