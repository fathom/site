import {SFC} from 'react'
import h from 'react-hyperscript'
import styled from 'styled-components'

import {Layout as MainLayout} from '../Layout'

export {Header} from './Header'
export {Image} from './Image'

export const Layout:SFC<{title: string}> = (props) => {
  return h(MainLayout, {title:props.title},
           h(Main, [props.children])
          )
}

const Main = styled('div')`
max-width: 800px;

h2 {
margin-top: 100px;
font-size: 1.75em;
}

img {
  max-width: 1000px;
  height: auto;
}

p {
  line-height: 1.6em;
}

blockquote {
font-style: italic;
padding-left: 10px;
border-left: solid 5px;
border-color: palevioletred;
}
`
