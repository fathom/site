import h from 'react-hyperscript'
import styled from 'styled-components'

import Link from 'next/link'

function importAll(r:any) {
  return r.keys().map(r);}
type Post = {
  meta: {
    title: string
    date: string
    blurb: string
    author?: string
    filename?: string
  },
  default: React.ComponentClass
}

const Posts:Post[] = importAll(
  //@ts-ignore
  require.context('../../pages/blog/', false, /(?!index)\.mdx$/)
);

export const List = () => {
  let postComponents = Posts.filter(post => post.meta)
    .sort((a, b) => {
      let aDate = new Date(a.meta.date)
      let bDate = new Date(b.meta.date)
      if (aDate > bDate) return -1
      return 1
    })
    .map((post) => {
      let href: string
      if(post.meta.filename) href = "/blog/" + post.meta.filename
      else href = "/blog/" + post.meta.title.toLowerCase().replace(/\s/g, '-')

      return h(PostContainer, {}, [
        h(DateSpan, post.meta.date),
        h(Title, post.meta.title),
        h(Blurb, post.meta.blurb),
        h(Link, {href}, h('a', "read more ->"))
      ])
    })

  return h('div', postComponents)
}

let Title = styled('h2')`
margin-top: 0px;
`

let DateSpan = styled('span')`
color: grey;
`

let PostContainer = styled('div')`
margin: 40px 0px;
`

let Blurb = styled('div')`
`
