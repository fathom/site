import h from 'react-hyperscript'
import styled from 'styled-components'


export const Header = (props:{title: string, date: string, blurb: string}) => {
  return h(HeaderBlock, [
    h(Title, props.title),
    h(DateDiv, props.date),
    h(Blurb, props.blurb)
  ])
}


const HeaderBlock = styled('div')`
margin-bottom: 30px;
`
const Title = styled('h1')`
margin-bottom: 5px;
`
const DateDiv = styled('div')`
color: grey;
`
const Blurb = styled('div')`
font-style: italic;
`
