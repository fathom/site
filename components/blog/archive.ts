import h from 'react-hyperscript'
import styled from 'styled-components'
import Link from 'next/link'

function importAll(r:any) {
  return r.keys().map(r);
}

type Post = {
  meta: {
    title: string
    date: string
    author?: string
    filename?: string
  },
  default: React.ComponentClass
}

export const Posts:Post[] = importAll(
  //@ts-ignore
  require.context('../../pages/blog/archive', true, /(?!index)\.mdx$/)
);

export const List = () => {
  let key = 0
  let postCompoents = Posts.filter(post => post.meta)
    .sort((a, b) => {
      let aDate = new Date(a.meta.date)
      let bDate = new Date(b.meta.date)
      if (aDate > bDate) return -1
      if(aDate.getTime() === bDate.getTime()) {
        if(a.meta.title > b.meta.title) return 1
        return -1
      }
      return 1
    })
    .map((post) => {
      key +=1
      let href: string
      if(post.meta.filename) href = "/blog/archive/" + post.meta.filename
      else href = "/blog/archive/" + post.meta.title.toLowerCase().replace(/\s/g, '-')

      return h('li', {key}, [
        h(Link, {href}, h('a', post.meta.title)),
        h(DateSpan, post.meta.date)
      ])
    })


  return h('ul', postCompoents)
}

let DateSpan = styled('span')`
margin-left: 20px;
color: grey;
`
