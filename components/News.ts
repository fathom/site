import styled from 'styled-components'
import h from 'react-hyperscript'

import {Layout as MainLayout} from './Layout'

export const Layout:React.SFC = (props) => {
  return h(MainLayout, {title: 'changelog'} ,[
    h(Main, [props.children])
  ])
}

const Main = styled('div')`
display: grid;
justify-items: center;
`

export const Entry:React.SFC<{date: string}> = (props) =>{
  let date = new Date(props.date).toLocaleDateString(undefined, {
    hour12: true,
    year: '2-digit',
    month: 'short',
    day: '2-digit'
  })
  return h(Container, [
    h(DateContainer, date ),
    h(Body, [props.children])
  ])
}

const Body = styled('div')`
width: 491px;
`

const DateContainer = styled('span')`
color: lightgrey;
`

const Container = styled('div')`
display:grid;
width: 600px;
grid-gap: 20px;
grid-template-columns: auto auto;
align-items: center;
justify-items: center;
`


