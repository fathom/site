# fathom Site (WIP):
This is the new fathom site, currently under construction.

## Getting Started:
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

Start by cloning down a copy of the repo:   
```
git clone https://gitlab.com/fathom/site.git
```

cd into it:   
```
cd site
```

Install dependencies:   
```
npm i
```

Serve up the site locally:   
```
npm run dev
```

And view the site locally by going to: `http://localhost:3000/`.


## Contributions
Read more about how we work with 
[CONTRIBUTIONS](https://gitlab.com/fathom/org/tree/master/CONTRIBUTING.md).

