---
title: Weekly Recap 4
date: 2017-10-15
---

Tad late again this week. 

## Chat rooms
We're currently working on a bit of a refactor of the chatroom codebase. 
Previously it was essentially a POC exploring the idea of a chat based 
environment for interacting with and around smart contracts. We're now taking 
that idea and putting it on a more solid foundation. This means handling state
properly in the app, improving data flow, and building a real front-end on top 
of some best practices.

A lot of this work is being led by Michiya Hibino who just joined the team on 
Monday. We're super excited to have him on board.

## Whitepaper

The whitepaper has now completed it's first iteration. It is still missing some
supplementary explanatory material, such as example assessment flows or a
glossary but it includes a complete description of the main architectural
components, a detailed walktrough of the protocol and the incentivization of the
its actors. Moreover, it lays out a roadmap for all the components that are
currently in the works and describes the first steps of how a fathom network
will be instantiated to successfully grow towards a critical mass such that it
will be self-sustainable and not vulnerable to malicious actors.

## Smart Contracts

Throughout the last two weeks, the team has worked on the remaining MVP-issues
that were crucial to ensure the correct incentivization of assessors.
Therefore we finished the issues outlined two weeks ago 


1. Redistributing funds from dissenting assessors ([merge request !65](https://gitlab.com/fathom/assess/merge_requests/65))

2. Refactoring token flow through assessments ([merge request !66](https://gitlab.com/fathom/assess/merge_requests/66))

3. Dealing with tied assessments ([merge request !64](https://gitlab.com/fathom/assess/merge_requests/64))

Moreover, we introduced a  [minimum size for assessments](https://gitlab.com/fathom/assess/merge_requests/64)
(minimum of five assessors), introduced a challenge period after the end of the 
commit phase and some smaller changes such as assuring changing seeds for the 
random calling of assessors.



## Force11 Conference

Julius is preparing a talk at the [Force11 conference](https://www.force11.org/), to talk about how
blockchain technology can codify social protocols. We'll try and make these
materials available if there's a need for them.

## Book Club!
You can find the discussion for the first book we're doing, Deschooling Society 
by Ivan Illich, [here](https://gitlab.com/fathom/fathom.gitlab.io/issues/12). 

While this will result in a post on a fixed time period the discussion can 
continue afterwards and the post will be updated to reflect it. We're still 
experimenting and thinking abotu how this can best work so this process could 
change.



