---
title: Prototyping away...
date: 2018-03-24
---

After agreeing on a vague
[roadmap](http://fathom.network/blog//2018-03-17-roadmap-reloaded.html), we have
been happily hacking on its first steps, leading to various prototypes of the
tools we eventually want to build:

#### Bridger

For bridger we built a small command line interface to create and verify claims,
with a first example linking an ethereum-address to a GitHub-identity. All it
takes for a GitHub-user to prove to users that he controls an ethereum address
is to post his address in a public gist. Metamask takes care of the signing and
IPFS of storage. Next up is turning it into a web-app and producing better
documentation. Visit [this](https://gitlab.com/fathom/bridger) repo to learn
more about it.

#### The fathom-app

Work has started on the fathom-app, our dedicated frontend for running and
managing fathom-assessments. We decided to use react-redux as a framework, which
we are now learning in the process. We aim to make that learning process and the
mini-prototypes we produce accessible and reproducible by keeping it well
documented and leaving trails of our decision-making by working issue-driven and
discussing a lot in writing (instead of verbally).

For now, we building the different components of the app independently of each
one another, starting with those two that will help us to experiment with
assessments the fastest: First, components to view and run assessments. Second,
components useful for creating and displaying activity (such as a user's call to
be an assessor or whether an assessment they participate in has moved to the
next stage). 

If you want to follow along check out these branches:
[dev-assessView](https://gitlab.com/fathom/fathom-app/tree/dev-assessView) and
[dev-dashboard](https://gitlab.com/fathom/fathom-app/tree/dev-dashboard).
Another piece of the app that it is in the back of our heads, yet not really
sketched out, is the concept-view which can be used to browse the concept-tree
and create assessments.

#### Testnets

With the fathom-app in its early stage, we needed some actual assessments to
interact with. So we now have a test-friendly version of our protocol (shorter
waiting times, no minimal number of assessors) on our [development
branch](https://gitlab.com/fathom/assess/tree/development) that can be deployed
to the kovan or rinkeby-testnet. Expect to read about some simulations soon!

#### Token-Dynamics

We finished implementing our token inflation mechanism. Basically, it is a
lottery that distributes new tokens in regular intervals to those users who have
risked their tokens by participating in assessments as assessors.

See this
[blogpost](http://fathom.network/blog//2018-01-05-distributing-tokens.html) for
a more detailed description and this [merge
request](https://gitlab.com/fathom/assess/merge_requests/94) for the code of the
minter-contract.

#### Mathetics Meetup

Heads up! Next monday will be the first iteration of our [mathetics
meetup](http://fathom.network/blog//2018-03-20-talking-about-learning.html): A
one-hour decentralized meetup where we and whoever is interested will discuss
different learning related topics ('mathetics' = science of learning). The first
topic will be *Talking to kids about the blockchain*.

#### We're on gitter!
Check out our [gitter-channel](https://gitter.im/fathom-network/Lobby), where we
are giving (almost) daily standups and discuss small issues concerning our
development process. Feel free to stop by, say hi, ask questions or tell us
about something cool and education related that has come across your way.
