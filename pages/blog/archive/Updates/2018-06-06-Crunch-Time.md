---
title: June Crunch Time
date: 2018-06-06
---

Lot's of little pieces adding up to some good momentum.

## A new team member!
For the next 2 months we're going to be joined by [Alex
Singh](https://www.alexsingh.com/) a multi-disciplinary designer and
entrepreneur.

Alex will be contributig to things across the board from the Playground to the
applications we're developing, bridger and the fathom-app!

## Running with the Mathetics meetup!
We recently had our second mathetics meetup, coincidentally also featuring Alex.
He gave a presentation on Mathetic primitivies, specifically **questions** and
**networks**, looking into how we can use them and how they work.

You can find his slides
[here](https://www.are.na/alex-singh/mathetics-primitives-presentation).

You can also see the recording for the first half of the presentation
[here](https://youtu.be/BbY0Pg-bC8Q) (sadly the recording didn't work for the
second half).

## Bridger rewrite
We finished up with the rewrite of the [core bridger
library](https://gitlab.com/fathom/bridger/blob/master/lib/Handler.ts) and nuked
our repository (if you look at it's [current
state](https://gitlab.com/fathom/bridger) it's pretty sparse)

This is because we've scrcapped the old UI and are currently working on
rewriting it as well! We've also scrapped the CLI (though we might bring it back
in the future)

You can take a look at some of our plans for the new UI in this
[issue](https://gitlab.com/fathom/bridger/issues/27) and keep track of our work
in this [milestone](https://gitlab.com/fathom/bridger/milestones/4)

## Process for the Playground

We've been iterating on several parts of the Playground, namely the [funding
process](https://github.com/fathom-playground/funding) and an [interview
series](https://github.com/fathom-playground/interviews).

The interviews are meant to provide some points of inspiration for learners in
the playground. If you have people who'd be awesome to talk to [please let us
know](mailto:contact@fathom.network)

Moving forward we will be creating a landing page for the playground and
focusing on improving the initial adventurer process to make it really easy for
people to join in.

## Transactions!

In the fathom-app we've been working on displaying transaction status easily in
the application. 

We've also been making a bunch of tweaks to the UI moving elements around. 

Next, the plan is to implement event-handling.
