---
title: Biweekly Recap
date: 2018-05-14
---

## Progress on the Tools

### Bridger

In the past 2 weeks, the focus has been on using
[TypeScript](https://www.typescriptlang.org/) in the core bridger library and
not just the app. We've been refactoring it significantly, and running into
classic Javascript problems of changing object shapes and function signatures.
Typescript is hugely helpful for this, and getting it deeply integrated into our
codebase has been awesome, giving us immediate feedback from the compiler when
we make changes. 

### Assessment App

We're making progress on the Assessment Interface. We have joined all the
components together and now we need to integrate style and improve the User
Experience by giving feedback on the user's actions.

- We ran an assessment with the Assessment Protocol App all the way to "reveal"
  stage, commiting the score in the cache of the browser.

- We added a theme and changing the style of buttons that the user can't
  interact with.

- We used Bounties to get a temporary Logo (it's very hard to choose between our
  [two candidates](https://consensys.bounties.network/bounty/v1/20) :'

### Playground

As the summer gets closer, we need to move forward with a clear description of
the Playground and finding funding for the grants.

- We worked with external people to make a clear description of the Playground
  (everybody is welcome to give feedback on the
  [repo](https://gitlab.com/fathom/playground) by opening a new issue)

- We used [Bounties](https://consensys.bounties.network/bounty/v1/19)
  and we got 2, which is a good start !

- We're developing our funding strategy in this
  [repo](https://github.com/fathom-playground/funding). It's a work in progress,
  but the goal is to have all the information we need to present to funders in
  one place.
