import {Layout, Header, Image} from '../../components/blog/'
import {Highlight} from '../../components/Highlight'

export const meta = {
  title: "Creating an alternative ed-tech",
  date: "2019-03-25",
  filename: "creating-an-alternative-edtech",
  author: "Jared Pereira",
  blurb: "How can we create meaningful technology for the future of learning?"
}

export default ({children}) => <Layout title={meta.title}>{children}</Layout>

<Header {...meta}/>

It's a pervasive — and not entirely untrue — meme that our education systems are
stuck in the past.

Talking points include: classrooms look the same as they did 200 years
ago, schools were designed to create factory workers, and they're modeled off
of assembly lines.

Never fear though. For as long as this idea's been around, there's been a
solution: **ed-tech**.

<Image caption="A popular image of ed-tech" src="/static/img/school-2000.jpg"/>

The goal is to usher education into the present with technology: software, apps,
computers, etc.

For a detailed picture read <Highlight>[Audrey Watter's
guide](http://guide.hackeducation.com/learners.html)</Highlight>. It's excellent
like all her writing on this topic.

On the surface this makes sense. The technology we have today gives us powerful
tools that have already reshaped much of society. We _can_ use them to change
education. But technology is not a panacea; what matters is **how** we use it.

<Highlight>Ed-tech hasn't used it well.</Highlight> If we look at the
incredibly pervasive impact technology has had on business, social networking,
or on other aspects of society in the last 50 years its effect on education
falls far short.

I'd like to make two points here: 

1. Ed-tech has failed to produce meaningful change because _the way it's
   produced_ is not in line with the needs of learners.
2. To create the change necessary, ed-tech needs to be made **by learners** as a
   deep part of their learning experience.

## How is ed-tech made?

First we have to look at the system for producing ed-tech. The archetype looks
something like this:

1. Someone or some company has some new idea for how learning can be different
2. They put together a team, maybe get some investment, and build a product
3. They sell this product to someone (administrators, teachers, parents, or
   _maybe_ learners)
4. The company listens users feedback, reconciles it with their business goals
   and their investors' goals, and iterates on the product

There are a lot of failure points in this process, but ultimately they all boil
down to the same thing: <Highlight>there's a disparity between the "product" and
the needs of learners.</Highlight>

The entities influencing the technology produced (i.e the companies developing and
the schools purchasing it) are all separated from the people actually using the
technology to learn.

A profit-driven company will prioritize data collection, or product lock-in, or
the latest buzzwords that attract that sweet investor capital.

A school may prioritize purchasing technology systems that help _them_ collect
data (aka surveillance), control students in a classroom, or turn classrooms
into Skinner's boxes to reward "good behavior". And then, the purchasing power
of schools leads directly to what companies create.

Think about something like [Google
Classroom](https://edu.google.com/products/classroom/?modal_active=none). It's
difficult to imagine a learner exerting any influence over that product. Though
it's provided to schools for free it's not run by a charity. It serves as part
of a business strategy to get people into the google ecosystem as early as
possible. That's the true purpose Classroom serves, one that has _nothing_ to do
with learning.

Again, people are pumping [billions](http://funding.hackeducation.com/) into products
like these, and yet the basic shape of schools and learning has _barely changed_.
This is because technology doesn't automatically create a better system, it
reinforces the systems that produce and control it.

### Doesn't this apply to all technology?

Of course. This is the same criticism that is applied to internet giants like
Facebook or Reddit. Their technology, apps and algorithms, are designed to serve
their (profit-driven) motives, and their customers (the advertisers), not yours
as a user.

The reason we've seen the value created in their fields is because the
incentives happen to align somewhat better with the needs of users. Connecting
more humans globally is a nice by-product of surveillance capitalism.

But with education, there are too many stakeholders for the incentives to be so
clear cut. It makes far more sense for a company to optimize for the needs of
the institutions before the learners.

Also, **education is _special_**. 

It's the mechanism by which people shape themselves and grow, and it's
incredibly dangerous when it's co-opted. Its impact propagates throughout
society, and flaws in it multiply and reinforce each other.

If in their learning people experience technology as something that controls
them, they'll take that association with them throughout their lives.

Ultimately this is a question of _agency_. Technology that serves goals other
than the learners' strips them of the freedom to shape themselves into who they
want to be, and build the world they want to.

## So what do we do?

We need a different way of producing technology for learning <Highlight>that
cannot be co-opted away from learners</Highlight>. There's really only one way
to achieve this: Learners need to be able to create and modify technology _for
themselves_.

This isn't as far-fetched as it sounds. Today we have powerful tools for
creating technology, whether you're building the latest app or building a
[satellite](http://www.cubesat.org/). And, we have an established philosophy and
movement that defends the rights of users to modify and extend the technology
they use.

### Free as in Freedom

The [Free Software
Movement](https://en.wikipedia.org/wiki/Free_software_movement) started by
Richard Stallman and championed by the [Free Software
Foundation](https://fsf.org) says this about free software:

>To use free software is to make a political and ethical choice asserting the
>right to learn, and share what we learn with others.

Free and open source software (FOSS) means that its "source code" is free to be
read, modified, and distributed by anyone. People have extended the idea to
[hardware](https://puri.sm/products/librem-5/) and even [social
rules](https://www.contributor-covenant.org/). FOSS software can be modified to
meet your specific needs, and those modifications can be shared back into the
community and benefit everyone.

It can encompass anything that falls under ed-tech. And for ed-tech to truly
impact and change education it _needs_ to be FOSS.

FOSS technology is fundamentally in the control of those who use it. Not only can
they make an informed decision about what the technology actually _does_ but
they can change it, and make it work for them. And importantly, they can *learn*
from it and use what they learn to create entirely new technology for
themselves. It changes the production of ed-tech from a profit-driven endeavor
to a public good that benefits everyone, like education and learning itself.

But this is still missing a key piece of the puzzle. Though the "source" may be
free for anyone to read and modify, **most people don't know how**. 

Computational literacy -- the ability to understand and create computational
systems -- is not a broadly distributed skill.

## Learn to create technology to learn

What we need is a way to create technology for learning <Highlight>that teaches
people how to produce technology for learning</Highlight>. This means that the
production of ed-tech needs to be deeply embedded in our learning institutions
and be fundamental to the learning process.

If someone encounters something they can't do, it should be a real option for
them to build the tools to let them do it. Diverting your education to learn
what you need in order to do that shouldn't be frowned upon, but actively
encouraged!

We need a system that treats creating technology like reading and writing, a
fundamental literacy that is embedded in all the ways people learn.

## What we're doing and what we need

We're trying to create this in [FNS](/fns).

It's a learning community where you can build the systems for learning that
make sense for you. The community is there to support you, contribute to what
you need, and grow with you. 

We're looking for people to pioneer this community: to be the first to
experiment with creating their own ed-tech so that others can use what they
produce and learn from their example. 

If that's you, consider [joining us](https://airtable.com/shr871jmo3Z2Ayfne).

## Reading List

The ideas in this post draw from a several different places:

- [Bret Victor](worrydream.com/)
- [Mindstorms](http://worrydream.com/refs/Papert%20-%20Mindstorms%201st%20ed.pdf)
- [Pedagogy of the Oppressed](https://en.wikipedia.org/wiki/Pedagogy_of_the_Oppressed)
- [Deschooling Society](https://en.wikipedia.org/wiki/Deschooling_Society)
