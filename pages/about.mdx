import {Layout} from '../components/Layout'
import {Main, Links, Intro, Block} from '../components/page'
import {Highlight} from '../components/Highlight'

export default ({children}) => <Layout title='about'>{children}</Layout>

<Main>
<Intro>

# About

Fathom is a system for learning.

It consists of tools and structures that anyone can use to create and develop
their learning experiences.

It's also the community of people who are building and using fathom to learn
together.

</Intro>

<Links>

[A System for Learning](#a-system-for-learning)

[Vision](#vision)

[History](#history)

[FAQ](#faq)

</Links>
</Main>

<Block>

# A system for learning

What is a system for learning? It's a collection of tools that work together to
let people learn. A tool could be software, or a process, or a book, or anything
else that supports people's learning.

We're heavily inspired by the [GNU operating system](https://www.gnu.org/).
It's a collection of software, from text-editors to games, that work together to
give users a computing environment that respects their freedom.

Fathom seeks do the same, but for one's learning environment. Learners should be
able to assemble their own "learning operating system" that supports them in
their specific goals. Each part of the system follows the [unix
philosophy](https://en.wikipedia.org/wiki/Unix_philosophy), of doing one thing
well and operating with others.

</Block>

<Block>

# Vision

<Highlight>Our vision is a world in which people are free to learn anything, 
in whatever way works best for them.</Highlight>

This may come off as glib or naive, so we'll try to break it down.

### _A world in which people are free to learn..._ 
First of all, everyone should have the freedom to learn. That means abundant
options, convenient access, and no artificial barriers.

Your opportunities to learn ought not be limited by factors such as geography,
race, gender, income, or age.

### _free to learn anything..._    
It's important those opportunities extend to _what_ people can learn. Ultimately
you should have the freedom to decide what to learn, informed by and in response
to the world around you. 

This freedom expands the possibilities for the connections and discoveries you
will make. As such, it contributes to the richness of our society.

### _...in whatever way works best_    
A core part of this is giving people agency over _how_ they learn things.
People acquire knowledge in different ways, informed by their prior knowledge,
their culture, the systems they have experience with, and countless other
factors. 

You might learn best via lectures, readings, or group study. But this sampling
barely scratches the surface, and it neglects many variables, nuances, and
combinations. How you learn best is uniquely personal, and the best way arrive
at it is to create it yourself, learning along the way.

</Block>

<Block>

# History

Fathom was conceived in late 2014 by Jared Pereira as an experiment to create an
alternative educational structure to schools.

Quickly this ran into the problem of assessment. In order to create structures
for learning, you need some way to capture or measure learning. So the summer of
2015 we began work on an assessment system. We drew inspiration from the
incentivized consensus systems emerging from the world of blockchain
technologies.

By summer 2016, a small prototype of the assessment smart contracts was ready
and out in the world. In September of that year came a whirlwind but pivotal 3
days in Shanghai at DevCon 2, the second annual Ethereum developers conference.
After a consequent meeting with Joe Lubin, the founder of blockchain technology
company [ConsenSys](https://consensys.net), Jared joined ConsenSys in October
2016.

Fathom was incubated within ConsenSys for just over two years. During that time
the team grew, learned an incredible amount, and built out our smart contract
system to be much more robust. We also worked on developing implementations to
get this thing out in the real world, and get people using it.

In 2019, we spun out from ConsenSys to continue work toward this vision.

</Block>

<Block>

# FAQ

### Is Fathom a proprietary learning system?
Fathom is an open system. It is not proprietary, but rather is free and open
source. That means you are able to use fathom tools and structures freely, and
for free. They are available for you to discover, use, extend, and share, as you
like.

As well, we welcome and encourage you to contribute any of _your_ ideas, tools,
and structures (including even things that originated here and that you've
modified) to improve the ways people everywhere can learn.

### Is Fathom anti-school?
No, we're not anti-school. We're pro learning. And we want to expand the
universe of options for learning. For learning anything, in any way. We think
that an open, decentralized, peer-connected model opens up a lot of exciting
possibilities. Fathom needn't exist in opposition to schools. And it could be
that one day, schools will look more like this.

### Why should people freely choose what to learn? Don't they need to be _taught_ what's important to learn?
This could turn into a circular argument, an infinite loop. And there are some
strong opinions out there about what people should learn and what they should be
taught. We're not legislators, and we're not out to dictate or enforce what or
how people should learn. We look at it in a different way -- we think that
people, when given ownership over their learning, make better and more
meaningful choices.

We also view learning not as an individually isolated endeavor, but one that
connects you to the world. Fathom is not designed to be a siloed playground for
self-directed focus, but an open social system in which everyone's learning is
strengthened by the social connections, resources, and diversity of the network.

###  I can learn about anything I want just using Google. How is Fathom different?
Information is more plentiful and accessible now than ever before. But our
_ways_ of learning, formally and intentionally, have not increased nearly as
much. We hope that Fathom will provide people with the tools, structures, and
social environment to engage in learning that is intentional, compelling, and
personally meaningful. We also hope that Fathom will help people to become
better learners.


### What age or type of learner is Fathom suited for?
That's difficult to say, and probably depends more on the learner than the age.
In these earlier days, fathom might favor those comfortable with some degree of
self-direction; and those interested in shaping and experimenting with their own
learning processes. Another characteristic could be those who feel constrained,
or underserved, and are seeking alternatives. But that is largely conjecture.

Fathom is being built for _everyone_, and we welcome _anyone_ to partake and contribute. Keep in mind that it's an evolving ecosystem of tools _and_ people. So it's strength and richness grows with your participation and input.


### Do you have any other questions about fathom?
Please reach out to us at [contact@fathom.network](mailto:contact@fathom.network).

</Block>


<Block>
</Block>


