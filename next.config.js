const slug = require('rehype-slug')
const withMDX = require('@zeit/next-mdx')({
  extension: /\.mdx?$/,
  options: {
    hastPlugins: [slug]
  }
})

module.exports = withMDX({
  pageExtensions:['ts', 'tsx', 'js', 'jsx', 'mdx']
})
